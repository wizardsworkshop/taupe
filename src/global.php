<?php

use Taupe\TaupeFactory;

use Taupe\Interfaces\Renderable;

function get_page() 
{
	return TaupeFactory::getPage();
}

function apnd(Renderable $elem)
{
	get_page()->append($elem);
}

// Dump and die, the old classic

function dd(...$vars)
{
	foreach ($vars as $var) {
		var_dump($var);
	}

	die;
}