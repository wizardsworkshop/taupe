<?php

namespace Taupe\Components;

/**
 * A generic text container element, display block, so we use a div.
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

class DivElement extends WebElement
{
	public function getTagName(): string
	{
		return 'div';
	}
}