<?php

namespace Taupe\Components;

/**
 * A generic paragraph element, display block
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

class ParagraphElement extends WebElement
{
	public function getTagName(): string
	{
		return 'p';
	}
}