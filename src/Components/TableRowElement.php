<?php

namespace Taupe\Components;

/**
 * A generic table row element
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

use Taupe\Interfaces\Renderable;

class TableRowElement extends WebElement
{
	public function __construct(array $rowVals)
	{
		foreach ($rowVals as $rv) {
			$this->append(new TableCellElement($rv));
		}		
	}

	public function append(Renderable $toAppend): void
	{
		if ($toAppend instanceof TableCellElement) {
			parent::append($toAppend);			
		} else {
			throw new \InvalidArgumentException('Only table cells can be appended to a table row');
		}
	}

	public function getTagName(): string
	{
		return 'tr';
	}
}