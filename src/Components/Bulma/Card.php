<?php

namespace Taupe\Components\Bulma;

/**
 * A Bulma CSS 'Card' representation.
 *
 *
 * Part of the Taupe Bulma package, the card is a general use media display area
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

use Taupe\Components\WebElement;
use Taupe\Components\DivElement;
use Taupe\Components\ParagraphElement;

class Card extends WebElement
{
	protected $classes = ['card'];
	
	/**
	 * Create the Bulma card instance
	 *
	 * @param string $title The title for the card
	 * @param string $body  The card's body text
	 */

	public function __construct(string $title, string $body = '')
	{
		$content = new DivElement;
		$content->addClass('card-content');
		
		$title = new ParagraphElement($title);
		$title->addClass('title');

		$content->append($title);

		$body = new ParagraphElement($body);
		$body->addClass('content');

		$content->append($body);

		$this->append($content);
	}

	public function getTagName(): string
	{
		return 'div';
	}
}