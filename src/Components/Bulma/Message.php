<?php

namespace Taupe\Components\Bulma;

/**
 * A Bulma CSS 'Message' representation.
 *
 *
 * Part of the Taupe Bulma package, the card is a general use message display, with option to close
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

use Taupe\Components\WebElement;
use Taupe\Components\DivElement;
use Taupe\Components\ParagraphElement;

class Message extends WebElement
{
	protected $classes = ['message', 'is-dark'];
	
	/**
	 * Create the Bulma card instance
	 *
	 * @param string $title The title for the card
	 * @param string $body  The card's body text
	 */

	public function __construct(string $header, string $body = '')
	{
		$title = new DivElement;
		$title->append(new ParagraphElement($header));
		$title->appendRaw('<button class="delete" aria-label="delete" onclick="this.parentNode.parentNode.remove();"></button>');
		$title->addClass('message-header');
	
		$this->append($title);

		$message = new DivElement($body);
		$message->addClass('message-body');

		$this->append($message);
	}

	public function getTagName(): string
	{
		return 'article';
	}
}