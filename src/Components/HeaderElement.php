<?php

namespace Taupe\Components;

/**
 * A generic section element
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

class HeaderElement extends WebElement
{
	/**
	 * @var int $level Level of the header, eg. `h1`, `h5`
	 */
	private $level;

	/**
	 * Create the Header, specifying which type (`$level`)
	 *
	 * @param int    $level Level of the header, eg. `h1`, `h5`
	 * @param string $body  Body of the header element
	 */

	public function __construct(int $level, string $body = '')
	{
		parent::__construct($body);
		$this->level = $level;
	}

	public function getTagName(): string
	{
		return 'h' . $this->level;
	}
}