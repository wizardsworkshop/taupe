<?php

namespace Taupe\Components;

/**
 * A representation of a full HTML document.
 *
 *
 * It acts as a wrapper for all the other web elements, and uses the 
 * `display` method to render the final document
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

use Taupe\Components\Stacks\StyleStack;
use Taupe\Components\Stacks\ScriptStack;

class WebPage extends WebElement
{
	private $head;
	private $title = 'Taupe Page';
	private $styles = [];

	private $scripts = [];

	/**
	 * Create the web page object
	 *
	 * @param string|null $title The title for the page
	 * @param string|null $body  Optional plain string body of the page
	 *
	 */

	public function __construct(string $title = null, string $body = null)
	{
		parent::__construct($body);

		if (null !== $title) {
			$this->setTitle($title);
		}
	}

	/**
	 * Display the final page!
	 *
	 * @return void
	 */

	public function display(): void
	{
		$styles = StyleStack::renderArr([
			'/demo/css/taupe-demo.css',
			'/demo/css/bulma.min.css'
		]);

		$scripts = ScriptStack::renderArr([
			'/demo/js/taupe-demo.js'
		]);

		echo 
<<<EOT
			<!doctype html>

				<html lang="en">
				<head>
				  <meta charset="utf-8">
				  {$styles}

				  <title>{$this->getTitle()}</title>
				  <meta name="description" content="A page description goes here">
				  <meta name="author" content="Taupe">

				</head>

				<body>
				  <div class="container">
				  	{$this->render()}
				  </div>
				  {$scripts}
				</body>
			</html>
EOT;

	}

	/**
	 * Set the page title
	 *
	 * @param string $title Title of the page
	 * @return void
	 */

	public function setTitle(string $title): void
	{
		$this->title = $title;
	}

	/**
	 * Get the page title
	 *
	 * @return string
	 */

	private function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @inheritdoc
	 */

	public function getTagName(): string
	{
		return 'html';
	}

}