<?php

namespace Taupe\Components;

/**
 * A generic text container element, display non-block, so we use a span.
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

class TextElement extends WebElement
{
	 /** 
	 * @var string $rawText Inner text of the element, not part of a child node
	 */

	private $rawText;

	public function __construct(string $text)
	{
		$this->rawText = $text;
	}

	public function render(): string
	{
		return $this->rawText;
	}

	public function getTagName(): string
	{
		return 'span';
	}

	public function appendRawText(string $toAppend): void
	{
		$this->rawText .= $toAppend;
	}
}