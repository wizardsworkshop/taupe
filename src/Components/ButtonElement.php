<?php

namespace Taupe\Components;

/**
 * A generic button element
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

class ButtonElement extends WebElement
{
	public function getTagName(): string
	{
		return 'button';
	}

	public function addOnClick(): void
	{
		
	}
}



// handleButtonClick