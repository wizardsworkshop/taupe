<?php

namespace Taupe\Components\Stacks;

use Taupe\Interfaces\Renderable;

/**
 * A basic flat stack of elements.
 * 
 * 
 * A faster to render, more static representation of a
 * non-wrapped collection of elements, in which we know
 * the exact structure of each element.
 * 
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

class ElementStack implements Renderable
{
	/**
	 * @var string[] $_stack The internal stack of values to render
	 */

	private $_stack;

	/**
	 * Get that stack!
	 *
	 * @param string[] $vals An array of values to 
	 */

	public function __construct(array $vals)
	{
		$this->_stack = $vals;
	}

	/**
	 * Render the full stack
	 *
	 * @return string
	 */

	public function render(): string
	{
		return static::renderArr($this->_stack);
	}

	/**
	 * Parse and render an array of strings
	 *
	 * @param string[] $arr Array of strings to parse
	 * @return string
	 */

	public static function renderArr(array $arr): string
	{
		$str = '';

		foreach ($arr as $val) {
			$str .= static::parse($val);
		}

		return $str;
	}

	/**
	 * Parse each stack value with this function for rendering
	 *
	 * @param string $val The value to parse
	 * @return string
	 */

	protected static function parse(string $val): string
	{
		throw new Error(print($errorStr = 'You should override the parse method when extending ElementStack!'));

		return $errorStr;
	}
}