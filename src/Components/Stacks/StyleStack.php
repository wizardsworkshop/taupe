<?php

namespace Taupe\Components\Stacks;

/**
 * A basic flat stack of style elements.
 * 
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */


class StyleStack extends ElementStack
{
	protected static function parse(string $val): string
	{
		return "<link rel=\"stylesheet\" type=\"text/css\" href=\"$val\">";
	}
}
