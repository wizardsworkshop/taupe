<?php

namespace Taupe\Components\Stacks;

/**
 * A basic flat stack of script elements.
 * 
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */


class ScriptStack extends ElementStack
{
	protected static function parse(string $val): string
	{
		return "<script type=\"text/javascript\" src=\"$val\"></script>";
	}
}
