<?php

namespace Taupe\Components;

/**
 * A generic table cell element
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

class TableCellElement extends WebElement
{
	public function getTagName(): string
	{
		return 'td';
	}
}