<?php

namespace Taupe\Components;

/**
 * A generic article element
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

class ArticleElement extends WebElement
{
	public function getTagName(): string
	{
		return 'article';
	}
}