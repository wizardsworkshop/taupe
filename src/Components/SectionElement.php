<?php

namespace Taupe\Components;

/**
 * A generic section element
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

class SectionElement extends WebElement
{
	public function getTagName(): string
	{
		return 'section';
	}
}