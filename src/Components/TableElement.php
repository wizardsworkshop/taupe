<?php

namespace Taupe\Components;

/**
 * A generic table element
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

use Taupe\Interfaces\Renderable;

class TableElement extends WebElement
{
	/**
	 * @var int $_colCount Internal count of columns
	 */
	private $_colCount;

	public function getTagName(): string
	{
		return 'table';
	}

	public function append(Renderable $toAppend): void
	{
		if ($toAppend instanceof TableRowElement) {
			parent::append($toAppend);			
		} else {
			throw new \InvalidArgumentException('Only table rows can be appended to a table');
		}
	}

	/**
	 * Add an array of row values to the table as a TableRowElement class
	 *
	 * @param string[] $rowVals Array of strings to add as a row
	 * @return void
	 */

	public function addRow(array $rowVals): void
	{
		if ( $this->hasBody() ) {
			// Check if the number of columns match
			
			if ($this->_colCount != count($rowVals)) {
				throw new \InvalidArgumentException('Wrong number of columns added to table!');
			}

		} else {
			$this->_colCount = count($rowVals);
		}

		$this->append(new TableRowElement($rowVals));
	}
}