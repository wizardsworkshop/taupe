<?php

namespace Taupe\Interfaces;

/**
 * A basic interface for a renderable class
 * 
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

interface Renderable
{
	public function render(): string;
}