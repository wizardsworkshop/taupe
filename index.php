<?php


spl_autoload_register(function($c) {
	$expl = explode('\\', $c);

	unset($expl[0]);
	
	require_once __DIR__ . '/src/' . implode('/', $expl) . '.php';
});

require './src/global.php';

use Taupe\TaupeFactory;

use Taupe\Components\TextElement;
use Taupe\Components\TableElement;
use Taupe\Components\Bulma\Card;
use Taupe\Components\Bulma\Message;

$page = TaupeFactory::createPage();
$page->append(new TextElement('This is some text<br>'));



$page->append(new Card('This is title', 'This is the body text and its a bit longer'));

$page->append(new Message('Messsage title', 'Body of the mssaaageeee'));

// for ($i = 0; $i < 11; $i++) {
// 	$page->append(new Card('Ayyyy titleeee', 'Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch Get bodied you lil bitch'));	
// }


$page->append(new Card('Card Title', 'Card Body Card Body Card Body Card Body Card Body '));

$table = new TableElement;

$table->addRow(['This', 'is', 'a', 'ROW']);
$table->addRow(['This', 'is', 'a', 'ROW']);

$page->append($table);

$page->display();

