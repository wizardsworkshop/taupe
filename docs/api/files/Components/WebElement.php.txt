<?php

namespace Taupe\Components;

use Taupe\Interfaces\Renderable;

/**
 * Representation of a generic web node (HTML element)
 * 
 * 
 * This is extended by all other element classes. It contains
 * the basic logic of rendering and wrapping text in tags.
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

abstract class WebElement implements Renderable
{
	/** 
	 * @var \Taupe\Interfaces\Renderable[] $bodyStack An array representing the child nodes of the element
	 */
	
	protected $bodyStack = [];

	 /** 
	  * @var bool $_hasRendered Wether or not the element has been rendered
	  */
	
	protected $_hasRendered = false;

	 /** 
	  * @var string|null $_renderText Cached rendered text
	  */
	
	protected $_renderText;

	/**
	 * @var string[] $classes A list of classes for the element
	 */

	protected $classes = [];

	/**
	 * Create a new element instance
	 *
	 * @param string $body Optional raw text of the body
	 */

	public function __construct(string $body = null)
	{
		if (null !== $body) {
			$this->appendRaw($body);
		}
	}

	/**
	 * Add an element to the body of the current element
	 * 
	 * @param \Taupe\Interfaces\Renderable $toAppend The element to append
	 * @return void
	 */

	public function append(WebElement $toAppend): void
	{
		$this->bodyStack[] = $toAppend;
	}

	/**
	 * Add some text to the raw text body of the element
	 *
	 * @param string $toAppend 
	 * @return void
	 */

	public function appendRaw(string $toAppend): void
	{
		$this->append(new TextElement($toAppend));
	}

	/**
	 * Render the element's child nodes and wrap in this class's tags
	 *
	 * @return string
	 */

	public function render(): string
	{
		if ($this->_hasRendered) {
			return $this->_renderText;
		}

		$str = '';

		foreach ($this->bodyStack as $elem) {
			$str .= $elem->render();
		}

		$this->_hasRendered = true;



		return $this->_renderText = $this->wrapInTag($str);
	}

	/**
	 * Check wether the element is already rendered
	 *
	 * @return bool
	 */

	public function hasRendered(): bool
	{
		return $this->_hasRendered;
	}

	/**
	 * Wrap text in the current element's tags
	 *
	 * @param string $text The text to wrap
	 * @return string
	 */

	public function wrapInTag(string $text): string
	{
		$tag = $this->getTagName();
		$id = bin2hex(random_bytes(4));
		$classes = implode(' ', $this->classes);

		return "<$tag id=\"{$id}\" class=\"$classes\">$text</$tag>";
	}

	/**
	 * Add a class to the element
	 *
	 * @param string $clss A string of a class / classes to add
	 * @return void
	 */

	public function addClass(string $clss)
	{
		if (false !== strpos($clss, ' ')) {
			$this->classes = array_merge(
				$this->classes,
				explode(' ', $clss)
			);
		} else {
			$this->classes[] = $clss;
		}
	}

	/**
	 * Get the tag name of this element
	 * 
	 * @return string
	 */

	abstract public function getTagName(): string;

	/**
	 * Parse the element to string
	 *
	 * @return string
	 */

	public function __toString()
	{
		return $this->render();
	}
}
