<?php

namespace Taupe;

use Taupe\Components\WebPage;

/**
 * This is a static interface to generate Taupe objects!
 *
 * @author Dan Nash <dan@wizardsworkshop.co.uk>
 * @copyright 2018 WizardsWorkshop Ltd.
 */

class TaupeFactory
{
	/**
	 * Create the initial web page with specified settings
	 *
	 * @var array|null Optional array of settings
	 * @return \Taupe\Components\WebPage
	 */

	public static function createPage(array $settings = null): WebPage
	{
		return new WebPage;
	}
}


